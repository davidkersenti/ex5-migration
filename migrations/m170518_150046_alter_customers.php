<?php

use yii\db\Migration;

class m170518_150046_alter_customers extends Migration
{
      public function up()
    {
		$this->addColumn('customers','description','string');
    }
    public function down()
    {
       $this->dropColumn('customers','description');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
